// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. 
//Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.


function problem3(inventory){
    
    let result = inventory.map(elem => elem.car_model).sort()
    
    return result
}

module.exports = {problem3}